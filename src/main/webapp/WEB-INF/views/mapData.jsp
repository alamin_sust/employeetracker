<%@ page import="java.util.ArrayList" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="com.employeetracker.connection.Database" %>
<%@ page import="com.employeetracker.web.util.Utils" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 22-May-18
  Time: 2:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Database db = new Database();
    db.connect();
    try {

        String query = request.getParameter("query"); //This will be received from an Ajax call

        Statement st = db.connection.createStatement();
        ResultSet rs = st.executeQuery(query);

        String ret = "S,";
        int rows = 1;

        int prevRows=0;

        if(!Utils.isEmplyOrNull(session.getAttribute("prevRows"))) {
            prevRows = Integer.parseInt(session.getAttribute("prevRows").toString());
        }

        String prevPoint="";
        while (rs.next()) {
            if(rows<prevRows) {
                rows++;
                continue;
            }
            if(rows==prevRows) {

                prevPoint = "_"+rs.getString("latitude")+"_"+rs.getString("longitude")+"_"+rs.getString("event_type")+"_"+rs.getString("event_time");
                rows++;
                continue;
            }
            if((rows-prevRows)>1) {
                ret+=",";
            }
            ret += rs.getString("latitude") + ",";
            ret += rs.getString("longitude") + ",";
            ret += rs.getString("event_type") + ",";
            ret += rs.getString("event_time");

            rows++;
        }

        session.setAttribute("prevRows",String.valueOf(rows-1));

        if(prevRows>0) {
            ret += "," + prevRows + prevPoint;
        } else {
            ret+=",0";
        }
        String json = new Gson().toJson(ret);
        response.setContentType("text/json");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(json);
    } catch (Exception e) {
        System.out.println(e);
    } finally {
        db.close();
    }
%>
