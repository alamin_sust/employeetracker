package com.employeetracker.service;

import com.employeetracker.connection.Database;
import com.employeetracker.web.util.Utils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Service
public class AppAuthService {



    public String processMultipartRequest(HttpServletRequest request, HttpSession session) throws FileUploadException, IOException, SQLException {

        Database db = new Database();
        String ret = "";
        db.connect();
        try {

            boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");

            String username = "";
            String password = "";
            String name = "";
            String designation = "";
            String branch = "";
            String user_type = "";
            Integer assigned_manager = 0;

            String insertProduct = "";
            String deleteProduct = "";
            String updateProductId = "";
            String uploadImage = "";




            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            List<InputStream> fileContentList = new LinkedList<>();
            boolean hasImage = false;

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString().replace("'","\\'");
                    // ... (do your job here)
                    if (fieldname.equals("username")) {
                        username = fieldvalue;
                    } else if (fieldname.equals("password")) {
                        password = fieldvalue;
                    } else if (fieldname.equals("name")) {
                        name = fieldvalue;
                    } else if (fieldname.equals("designation")) {
                        designation = fieldvalue;
                    } else if (fieldname.equals("branch")) {
                        branch = fieldvalue;
                    } else if (fieldname.equals("user_type")) {
                        user_type = fieldvalue;
                    } else if (fieldname.equals("manager")&&!Utils.isEmplyOrNull(fieldvalue)) {
                        assigned_manager = Integer.valueOf(fieldvalue);
                    } else if (fieldname.equals("insert")) {
                        insertProduct = fieldvalue;
                    } else if (fieldname.equals("delete")) {
                        deleteProduct = fieldvalue;
                    } else if (fieldname.equals("updateProductId")) {
                        updateProductId = fieldvalue;
                    } else if (fieldname.equals("uploadImage")) {
                        uploadImage = fieldvalue;
                    }
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    //String filename = FilenameUtils.getName(item.getName());

                    if (fieldname.startsWith("file")) {
                        fileContentList.add(item.getInputStream());
                        hasImage = true;
                    }
                }
            }

            //validation


            Statement stL = db.connection.createStatement();
            String qL = "select max(id)+1 as mxId from user";
            ResultSet rsL = stL.executeQuery(qL);
            rsL.next();

            if (hasImage) {
                // ... (do your job here)
//                        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
//                        Calendar cal = Calendar.getInstance();
                //System.out.println(dateFormat.format(cal.getTime()));

                for (int i = 0; i < fileContentList.size(); i++) {

                    InputStream fileContent = fileContentList.get(i);

                    String postFix = "";

                    if (i != 0) {
                        postFix += "-" + (i + 1);
                    }



                    String picId=(updateProductId.equals("")?rsL.getString("mxId"):updateProductId);

                    if(!uploadImage.equals("")) {
                        picId=uploadImage;
                    }

                    File bfile = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\EmployeeTracker\\src\\main\\webapp\\resources\\assets\\images\\" + picId + ".jpg");
                    File bfileBuild = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\EmployeeTracker\\build\\libs\\exploded\\EmployeeTracker.war\\resources\\assets\\images\\" + picId + ".jpg");
                    //out.print(getServletContext().getRealPath("/"));
                    OutputStream outStream = new FileOutputStream(bfile);
                    OutputStream outStreamBuild = new FileOutputStream(bfileBuild);

                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = fileContent.read(buffer)) > 0) {
                        outStream.write(buffer, 0, length);
                        outStreamBuild.write(buffer, 0, length);
                    }
                    fileContent.close();
                    outStream.close();
                    outStreamBuild.close();
                }


                //String query2 = "insert into pillar_updated_by(soldier_id,pillar_id,img_url,longitude,latitude,battalion,imie_number) values(" + soldierId + ","+id+", '" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg" + "','"+longitude+"','"+latitude+"','"+ ConverterUtil.getBattalionFromImie(authImie)+"','"+authImie+"')";

                //Statement st2 = db.connection.createStatement();

                //st2.executeUpdate(query2);
            }


            //String query = "insert into pillar(id,name,number,longitude,latitude,situation) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"', '"+situation+"')";


            if(!Utils.isEmplyOrNull(insertProduct)) {
                if(isAdmin) {
                    Statement st = db.connection.createStatement();
                    String q = "insert into user (id,username,password,name,designation,branch,user_type,assigned_manager) values("+rsL.getString("mxId")+",'"+username+"','"+password+"','"+name+"','"+designation+"','"+branch+"','"+user_type+"',"+(assigned_manager==0?session.getAttribute("id"):assigned_manager)+")";
                    st.executeUpdate(q);
                    session.setAttribute("sm", "Successfully Inserted into Database!");
                    ret=rsL.getString("mxId");
                }
            }

            if(!Utils.isEmplyOrNull(deleteProduct)) {

                Statement st = db.connection.createStatement();
                String q = "delete from user where id="+deleteProduct;
                st.executeUpdate(q);
                session.setAttribute("sm", "Successfully Deleted from Database!");
            }

            if(!Utils.isEmplyOrNull(updateProductId)) {

                if(updateProductId.equals("1")){
                    user_type="admin";
                    assigned_manager=1;
                }

                Statement st = db.connection.createStatement();
                String q = "update user set username = '"+username+"',password='"+password+"',name='"+name+"',designation='"+designation+"',branch='"+branch+"',user_type='"+user_type+"',assigned_manager="+(assigned_manager==0?session.getAttribute("id"):assigned_manager)+" where id="+updateProductId;
                st.executeUpdate(q);
                session.setAttribute("sm", "Successfully Updated!");
                ret = updateProductId;
            }

        } catch (Exception e) {
            ret = "";
        } finally {
            db.close();
        }
        return ret;
    }
}
