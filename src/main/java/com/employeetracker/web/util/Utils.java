package com.employeetracker.web.util;

import com.employeetracker.connection.Database;

import javax.rmi.CORBA.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static boolean isEmplyOrNull(Object string) {
        return string == null || string.toString().equals("");
    }

    public static String getTimeStrNow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

    public static double getDistanceInKm(double lat1,double lon1,
                                         double lat2, double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance)/1000.0;
    }

/*
    private static ResultSet executeQuery(String q) throws SQLException {

        ResultSet rs = null;
        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            rs = st.executeQuery(q);
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            //db.close();
        }
        return rs;
    }

    public static int getTotalOnline() throws SQLException {
        ResultSet rs = executeQuery("select distinct user_id as cnt from event where substr(event_time,1,16)>='"+ Utils.getTimeStrNow()+"'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalOnline(String managerId) throws SQLException {
        ResultSet rs = executeQuery("select distinct user_id as cnt from event where substr(event_time,1,16)>="+ Utils.getTimeStrNow());
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalManagers() throws SQLException {
        ResultSet rs =  executeQuery("select count(*) as cnt from user where user_type='manager'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalEmployees() throws SQLException {
        ResultSet rs = executeQuery("select count(*) as cnt from user where user_type='employee'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static ResultSet getEmployees(String managerId) throws SQLException {
        return executeQuery("select * from user where  user_type='employee' and assigned_manager="+managerId);
    }

    public static ResultSet getManagers() throws SQLException {
        return executeQuery("select * from user where  user_type='manager'");
    }
*/


}
