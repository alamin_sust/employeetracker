package com.employeetracker.web.controller;

import com.employeetracker.service.AppAuthService;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@Controller
public class HomeController {

    @Autowired
    private AppAuthService appAuthService;

    @GetMapping("/")
    public String mainGet() {
        return "login";
    }

    @GetMapping("/login")
    public String loginGet() {
        return "login";
    }

    @PostMapping("/login")
    public String loginPost() {
        return "login";
    }

    @GetMapping("/mapData")
    public String mapDataGet() {
        return "mapData";
    }

    @PostMapping("/mapData")
    public String mapDataPost() {
        return "mapData";
    }

    @GetMapping("/tracking")
    public String trackingGet() {
        return "tracking";
    }

    @PostMapping("/tracking")
    public String trackingPost() {
        return "tracking";
    }

    @GetMapping("/history")
    public String historyGet() {
        return "history";
    }

    @PostMapping("/history")
    public String historyPost() {
        return "history";
    }

    @GetMapping("/dashboard")
    public String dashboardGet() {
        return "dashboard";
    }

    @PostMapping("/dashboard")
    public String dashboardPost() throws FileUploadException, SQLException, IOException {

        return "dashboard";
    }

    @GetMapping("/dashboard2")
    public String dashboardGet2() {
        return "dashboard2";
    }

    @PostMapping("/dashboard2")
    public String dashboardPost2() throws FileUploadException, SQLException, IOException {

        return "dashboard2";
    }

    @GetMapping("/loginMobile")
    public String loginMobileGet() {
        return "loginMobile";
    }

    @PostMapping("/loginMobile")
    public String loginMobilePost() {
        return "loginMobile";
    }

    @GetMapping("/event")
    public String eventGet() {
        return "event";
    }

    @PostMapping("/event")
    public String eventPost() {
        return "event";
    }

    @GetMapping("/reports")
    public String reportsGet() {
        return "reports";
    }

    @PostMapping("/reports")
    public String reportsPost() {
        return "reports";
    }

    @GetMapping("/account")
    public String accountGet() {
        return "account";
    }

    @PostMapping("/account")
    public String accountPost() {
        return "account";
    }

    @GetMapping("/account-info")
    public String accountIGet() {
        return "account-info";
    }

    @PostMapping("/account-info")
    public String accountIPost() {
        return "account-info";
    }

    @GetMapping("/live-tracking")
    public String lTGet() {
        return "live-tracking";
    }

    @PostMapping("/live-tracking")
    public String lTPost() {
        return "live-tracking";
    }

    @GetMapping("/live-tracking2")
    public String lT2Get() {
        return "live-tracking2";
    }

    @PostMapping("/live-tracking2")
    public String lT2Post() {
        return "live-tracking2";
    }

    @GetMapping("/profile")
    public String profileGet() throws FileUploadException, SQLException, IOException {

        return "profile";
    }

    @PostMapping("/profile")
    public String profilePost(HttpServletRequest request, HttpSession session) throws FileUploadException, SQLException, IOException {
        String ret = appAuthService.processMultipartRequest(request,session);
        return "account-info";
    }
}
