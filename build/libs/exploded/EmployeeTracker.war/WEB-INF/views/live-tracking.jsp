<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.employeetracker.web.util.Utils" %>
<%@ page import="com.employeetracker.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="javax.rmi.CORBA.Util" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- metisMenu.min.css -->
    <link rel="stylesheet" href="resources/assets/css/metisMenu.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
            setInterval(function() {
                /*$('#maindiv').load('tracking');*/
                history.go(0);

            }, 20000); // the "3000"
        });




        var rad = function(x) {
            return x * Math.PI / 180;
        };

        var getDistance = function(lat1, lng1, lat2, lng2) {
            var R = 6378137; // Earth’s mean radius in meter
            var dLat = rad(lat2 - lat1);
            var dLong = rad(lng2 - lng1);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d; // returns the distance in meter
        };


        function populateManager() {
            window.location.href='live-tracking?manager='+document.getElementById("manager").value+"&employee="+document.getElementById("employee").value;
        }

        function populateEmployee() {
            window.location.href='live-tracking?manager='+document.getElementById("manager").value+"&employee="+document.getElementById("employee").value;
        }
    </script>
</head>

<body onload="initialize();">
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");



%>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header-area start -->
<header class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                <div class="header-left">
                    <%if(isAdmin){%>
                    <h5><a href="dashboard"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}else{%>
                    <h5><a href="dashboard2"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}%>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 col-8">
                <div class="header-right text-right">
                    <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 d-block d-lg-none col-4">
                <ul class="menu">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-12">
            <!-- sidebar-style start -->
            <div class="sidebar-style">
                <div class="sliderbar-area">
                    <h2 class="slidebar-title">Hi <%=session.getAttribute("username")%></h2>
                    <div class="profile-img">
                        <img src="resources/assets/images/<%=session.getAttribute("id")%>.jpg" alt="Profile Picture">
                        <div class="profile-content">
                            <form method="post" action="profile" enctype="multipart/form-data">
                                <input type="hidden" name="uploadImage" value="<%=session.getAttribute("id")%>"/>
                                <input type="file" name="file" required/>
                                <button type="submit"><i class="fa fa-camera"></i>Upload Image</button>
                            </form>
                        </div>
                    </div>
                    <div class="mainmenu">
                        <ul>
                            <%if(isAdmin){%>
                            <li ><a href="dashboard">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li class="active"><a href="live-tracking">Live Tracking</a></li>
                            <%}else {%>
                            <li><a href="dashboard2">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li class="active"><a href="live-tracking2">Live Tracking</a></li>
                            <%}%>
                            <li><a href="reports">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- sidebar-style end -->
        </div>
        <div class="col-xl-10 col-lg-9 col-12">
            <div class="main-content">
                <div class="search-wrapper contact-history">
                    <ul class="d-flex search-wrapper justify-content-start">
                        <%--<li><h5>Select Managers</h5></li>--%>
                        <li class="select-form">
                            <%


                                Statement st4 = db.connection.createStatement();
                                ResultSet rs4 = st4.executeQuery("select * from user where  user_type='manager'");

                            %>
                            <select class="js-example-basic-single" id="manager" name="manager" onchange="populateManager();">
                                <option <%if(request.getParameter("manager")==null||request.getParameter("manager").equals("")){%>selected<%}%> value="">Select Manager</option>
                                <%while (rs4.next()) {%>

                                <option <%if(request.getParameter("manager")!=null&&request.getParameter("manager").equals(rs4.getString("id"))){%>selected<%}%> value="<%=rs4.getString("id")%>"><%=rs4.getString("name")%></option>
                                <%}%>
                            </select>
                        </li>
                    </ul>
                    <ul class="d-flex search-wrapper justify-content-start">
                        <%--<li><h5>Select Employee</h5></li>--%>
                        <li class="select-form">
                            <%


                                String exQ="";
                                if(!Utils.isEmplyOrNull(request.getParameter("manager"))) {
                                    exQ+=" and assigned_manager="+request.getParameter("manager");
                                }

                                Statement st5 = db.connection.createStatement();
                                ResultSet rs5 = st5.executeQuery("select * from user where  user_type='employee'"+exQ);

                            %>
                            <select class="js-example-basic-single" id="employee" name="employee" onchange="populateEmployee();">
                                <option <%if(request.getParameter("employee")==null||request.getParameter("employee").equals("")){%>selected<%}%> value="">Select Employee</option>
                                <%while (rs5.next()) {%>

                                <option <%if(request.getParameter("employee")!=null&&request.getParameter("employee").equals(rs5.getString("id"))){%>selected<%}%> value="<%=rs5.getString("id")%>"><%=rs5.getString("name")%></option>
                                <%}%>
                            </select>
                        </li>
                    </ul>
                </div>


                <%
                    String managerId="-1";
                    String employeeId="-1";

                    if(!Utils.isEmplyOrNull(request.getParameter("manager"))) {
                        managerId=request.getParameter("manager");
                    }
                    if(!Utils.isEmplyOrNull(request.getParameter("employee"))) {
                        employeeId=request.getParameter("employee");
                    }

                    String q = "select * from event join user on(user.id=event.user_id) where" +
                            " user.assigned_manager="+managerId+" and user.id="+employeeId;

                    q+=" and substr(event_time,1,10) like '"+Utils.getTimeStrNow().substring(0,10)+"' ";
                    q+=" ORDER by event_time asc";

                    session.setAttribute("prevRows", "0");

                %>

                <%--<div id="map"></div>--%>

                <script>

                    var markerCount = 0;
                    var map;

                    var gmarkers = Array();

                    //Initializes the map…
                    function initialize() {
                        var myLatlng = new google.maps.LatLng(23.363, 90.044);
                        var map_canvas = document.getElementById('contact-google-map2');
                        var map_options = {
                            center: myLatlng,
                            zoom: 13
                        };
                        map = new google.maps.Map(map_canvas, map_options);

                        loadData();
                    }

                    function addMarkerToMap(lat, long, isFirstOrLastUrl, htmlMarkupForInfoWindow){
                        var infowindow = new google.maps.InfoWindow();
                        var myLatLng = new google.maps.LatLng(lat, long);
                        var url = "resources/assets/images/<%=request.getParameter("employee")%>.jpg";
                        if(isFirstOrLastUrl === false) {
                            url="";
                        }

                        var icon = {
                            url: url, // url
                            scaledSize: new google.maps.Size(40, 40), // scaled size
                            origin: new google.maps.Point(0, 0), // origin
                            anchor: new google.maps.Point(0, 0) // anchor
                        };

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            icon: icon
                        });

                        //Gives each marker an Id for the on click
                        markerCount++;

                        gmarkers.push(marker);
                        //Creates the event listener for clicking the marker
                        //and places the marker on the map
                        google.maps.event.addListener(marker, 'click', (function(marker, markerCount) {
                            return function() {
                                infowindow.setContent(htmlMarkupForInfoWindow);
                                infowindow.open(map, marker);
                            }
                        })(marker, markerCount));

                        //Pans map to the new location of the marker
                        map.panTo(myLatLng)
                    }

                    function makePath(lat1, long1, lat2, long2) {
                        var flightPath = new google.maps.Polyline({
                            path: [{lat:lat1, lng:long1},
                                {lat:lat2, lng:long2}],
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        flightPath.setMap(map);
                    }

                    function resetMarkers() {
                        for(i = 0 ; i< gmarkers.length; i++) {
                            if(i>0 && i<gmarkers.length-1) {
                                gmarkers[i].setVisible(false);
                            }
                        }
                    }

                    function loadData() {
                        var rows=0;

                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                //document.getElementById("demo").innerHTML = this.responseText;

                                var ret=this.responseText;
                                var array = ret.replace('"','').split(",");

                                var i=1;
                                var prevLat=24.2,prevLong=82.2;
                                var prevPoints = false;
                                if(array[array.length-1].charAt(0)!=='0') {
                                    var array2 = array[array.length-1].split("_");
                                    prevLat = array2[1];
                                    prevLong = array2[2];
                                    prevPoints=true;
                                }
                                var distCovered = 0.0;
                                while ((i+2)<array.length) {
                                    rows++;

                                    var lat = array[0+i];
                                    var long = array[1+i];
                                    var eventType = array[2+i];
                                    var eventTime = array[3+i];


                                    if (i > 1 || prevPoints) {
                                        distCovered += getDistance(prevLat, prevLong, lat, long);
                                        if (eventType === "active" ||
                                            eventType === "end" || eventType === "pause") {
                                            makePath(parseFloat(prevLat),parseFloat(prevLong), parseFloat(lat),parseFloat(long));
                                        }
                                        else {
                                            distCovered = 0;
                                        }
                                    }

                                    var isFirstOrLastUrl = false;
                                    if((i===1 && prevPoints===false) || (i+4+1)>=array.length) {
                                        isFirstOrLastUrl = true;
                                    }
                                    addMarkerToMap(lat, long, isFirstOrLastUrl, "<strong> time : "+ eventTime+"</strong>"+"<br >"+ " <b>event : "+ eventType+"</b> <br><b> Distance Covered: "+ distCovered+" meters </b>");
                                    resetMarkers();
                                    prevLat = lat;
                                    prevLong= long;
                                    i=i+4;
                                }
                            }
                        };
                        xhttp.open("GET", "mapData?query=<%=q%>", true);
                        xhttp.send();
                    }
                    setInterval(loadData, 10000);
                </script>


                <div class="col-xl-10 col-lg-9">
                    <div id="contact-google-map2"></div>
                </div>



            </div>
        </div>
    </div>
</div>
<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- Google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/gmap.js"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/map-script.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>

</html>
