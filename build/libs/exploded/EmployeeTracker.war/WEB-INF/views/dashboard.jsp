<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.employeetracker.web.util.Utils" %>
<%@ page import="com.employeetracker.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="javax.rmi.CORBA.Util" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="resources/assets/images/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script>
        function populateManager() {
            /*document.getElementById("managerId").value=document.getElementById("manager").value;*/
            window.location.href='dashboard?managerId='+document.getElementById("manager").value;
        }
    </script>
</head>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");




%>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header-area start -->
<header class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                <div class="header-left">
                    <%if(isAdmin){%>
                    <h5><a href="dashboard"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}else{%>
                    <h5><a href="dashboard2"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}%>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 col-8">
                <div class="header-right text-right">
                    <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 d-block d-lg-none col-4">
                <ul class="menu">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-12">
            <!-- sidebar-style start -->
            <div class="sidebar-style">
                <div class="sliderbar-area">
                    <h2 class="slidebar-title">Hi <%=session.getAttribute("username")%></h2>
                    <div class="profile-img">
                        <img src="resources/assets/images/<%=session.getAttribute("id")%>.jpg" alt="Profile Picture">
                        <div class="profile-content">
                            <form method="post" action="profile" enctype="multipart/form-data">
                                <input type="hidden" name="uploadImage" value="<%=session.getAttribute("id")%>"/>
                                <input type="file" name="file" required/>
                                <button type="submit"><i class="fa fa-camera"></i>Upload Image</button>
                            </form>
                        </div>
                    </div>
                    <div class="mainmenu">
                        <ul>
                            <%if(isAdmin){%>
                            <li class="active"><a href="dashboard">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li><a href="live-tracking">Live Tracking</a></li>
                            <%}else {%>
                            <li class="active"><a href="dashboard2">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li><a href="live-tracking2">Live Tracking</a></li>
                            <%}%>
                            <li><a href="reports">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- sidebar-style end -->
        </div>
        <div class="col-xl-10 col-lg-9 col-12">
            <div class="main-content">
                <div class="deshbord-history">
                    <h2 class="section-title">Deshbord<span> Live </span></h2>
                    <div class="row">
                        <div class="col-lg-4 col-xl-3 col-sm-6 col-12">
                            <div class="deshbord-wrap">
                                <%
                                    Statement st2 = db.connection.createStatement();
                                    ResultSet rs2 = st2.executeQuery("select count(*) as cnt from user where user_type='employee'");
                                    rs2.next();
                                %>
                                <h3 class="counter"><%=rs2.getString("cnt")%></h3>
                                <h4>Total Employees</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 col-sm-6 col-12">
                            <div class="deshbord-wrap deshbord-color1">
                                <%
                                    Statement st = db.connection.createStatement();
                                    ResultSet rs = st.executeQuery("select count(distinct user_id) as cnt from event where substr(event_time,1,16)>='"+ Utils.getTimeStrNow()+"'");
                                    rs.next();
                                %>
                                <h3 class="counter"><%=rs.getString("cnt")%></h3>
                                <h4>Online</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 col-sm-6 col-12">
                            <div class="deshbord-wrap deshbord-color2">
                                <%
                                    Statement st3 = db.connection.createStatement();
                                    ResultSet rs3 = st3.executeQuery("select count(*) as cnt from user where user_type='manager'");
                                    rs3.next();
                                %>
                                <h3 class="counter"><%=rs3.getString("cnt")%></h3>
                                <h4>Total Managers</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="search-wrapper">

                    <ul class="d-flex search-wrapper">
                        <%--<li><h5>Select Managers</h5></li>--%>
                        <li class="select-form">
                            <%
                                Statement st4 = db.connection.createStatement();
                                ResultSet rs4 = st4.executeQuery("select * from user where  user_type='manager'");

                            %>
                            <select style="width: 650px;" class="js-example-basic-single" id="manager" name="manager" onchange="populateManager();">
                                <option disabled selected value="">Select Manager</option>
                                <%while (rs4.next()) {%>

                                <option value="<%=rs4.getString("id")%>"><%=rs4.getString("name")%></option>
                                <%}%>
                            </select>
                        </li>
                    </ul>
                    <ul class="d-flex">

                        <%--<li><h5>Employes</h5></li>--%>
                        <li class="search-form">
                            <form action="dashboard" method="get">
                                <input type="text" name="employeeName" placeholder="Search here...">
                                <%--<input id="managerId" type="hidden" name="managerId" value="">--%>
                                <button type="submit">search</button>
                        </form>
                        </li>
                    </ul>

                </div>

                <div class="table-responsive table-style">
                    <table>
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Id</th>
                            <th>Designation</th>
                            <th>Branch</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            int cnt=1;
                            Statement st5 = db.connection.createStatement();

                            String exQ="";
                            String urlParam="";
                            if(!Utils.isEmplyOrNull(request.getParameter("managerId"))) {
                                exQ += " and assigned_manager="+request.getParameter("managerId");
                                    urlParam+="&manager="+request.getParameter("managerId");


                            }
                            if(!Utils.isEmplyOrNull(request.getParameter("employeeName"))) {

                                exQ += " and name like '%"+request.getParameter("employeeName")+"%'";

                            }

                            ResultSet rs5 = st5.executeQuery("select * from user where  user_type='employee'"+exQ);

                            while (rs5.next()) {

                        %>
                        <tr>
                            <td><%=cnt++%></td>
                            <td><img style="width: 100px;height: 80px;" src="resources/assets/images/<%=rs5.getString("id")%>.jpg"></td>
                            <td><%=rs5.getString("name")%></td>
                            <td><%=rs5.getString("id")%></td>
                            <td><%=rs5.getString("designation")%></td>
                            <td><%=rs5.getString("branch")%></td>
                            <td class="link">
                                <ul>
                                    <%if(isAdmin){%>
                                    <li><a href="live-tracking?employee=<%=rs5.getString("id")%><%=urlParam%>">Track</a></li>
                                    <li><a href="reports">History</a></li>
                                    <%} else {%>
                                    <li><a href="live-tracking2?employee=<%=rs5.getString("id")%>">Track</a></li>
                                    <li><a href="reports">History</a></li>
                                    <%}%>
                                </ul>
                            </td>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>
</body>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</html>