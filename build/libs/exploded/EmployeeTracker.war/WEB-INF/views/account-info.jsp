<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.employeetracker.web.util.Utils" %>
<%@ page import="com.employeetracker.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="javax.rmi.CORBA.Util" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="resources/assets/images/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script>
        function showOrHideManager() {
            if(document.getElementsByName("user_type")[0].value === "employee") {
                document.getElementById("managerD").style.display = "block";
            } else {
                document.getElementById("managerD").style.display = "none";
            }
        }

        function populateManager() {
            window.location.href='account-info?manager='+document.getElementById("manager").value;
        }
    </script>
</head>

<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");

        String id;
        String q= "select * from user where id=";
        Statement st = db.connection.createStatement();
        ResultSet rs = null;

        String name = "";
        String username = "";
        String password = "";
        String designation = "";
        String branch = "";
        String user_type = "";
        String assigned_manager = "";


        if(!Utils.isEmplyOrNull(request.getParameter("id")))
        {
            id=request.getParameter("id");
            q+=id;
            if(!isAdmin && !(!Utils.isEmplyOrNull(session.getAttribute("id"))&&id.equals(session.getAttribute("id")))) {
                q += " and assigned_manager=" + session.getAttribute("id");
            }
            rs = st.executeQuery(q);
            if(!rs.next()) {
                response.sendRedirect("dashboard");
            }

            name = rs.getString("name");
            username = rs.getString("username");
            password = rs.getString("password");
            designation = rs.getString("designation");
            branch = rs.getString("branch");
            user_type = rs.getString("user_type");
            assigned_manager = rs.getString("assigned_manager");
        } else {
            id= session.getAttribute("id").toString();

        }



%>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header-area start -->
<header class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                <div class="header-left">
                    <%if(isAdmin){%>
                    <h5><a href="dashboard"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}else{%>
                    <h5><a href="dashboard2"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}%>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 col-8">
                <div class="header-right text-right">
                    <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 d-block d-lg-none col-4">
                <ul class="menu">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-12">
            <!-- sidebar-style start -->
            <div class="sidebar-style">
                <div class="sliderbar-area">
                    <h2 class="slidebar-title">Hi <%=session.getAttribute("username")%></h2>
                    <div class="profile-img">
                        <img src="resources/assets/images/<%=session.getAttribute("id")%>.jpg" alt="Profile Picture">
                        <div class="profile-content">
                            <form method="post" action="profile" enctype="multipart/form-data">
                                <input type="hidden" name="uploadImage" value="<%=session.getAttribute("id")%>"/>
                                <input type="file" name="file" required/>
                                <button type="submit"><i class="fa fa-camera"></i>Upload Image</button>
                            </form>
                        </div>
                    </div>
                    <div class="mainmenu">
                        <ul>
                            <%if(isAdmin){%>
                            <li ><a href="dashboard">Dashboard</a></li>
                            <li class="active"><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li><a href="live-tracking">Live Tracking</a></li>
                            <%}else {%>
                            <li ><a href="dashboard2">Dashboard</a></li>
                            <li class="active"><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li><a href="live-tracking2">Live Tracking</a></li>
                            <%}%>
                            <li><a href="reports">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- sidebar-style end -->
        </div>
        <div class="col-xl-10 col-lg-9 col-12">
            <div class="main-content">

                <div class="search-wrapper">

                    <% if(isAdmin) {%>
                    <ul class="d-flex search-wrapper">
                        <%--<li><h5>Select Managers</h5></li>--%>
                        <li class="select-form">
                            <%
                                Statement st4 = db.connection.createStatement();
                                ResultSet rs4 = st4.executeQuery("select * from user where  user_type='manager'");

                            %>
                            <select class="js-example-basic-single" id="manager" name="manager" onchange="populateManager();">
                                <option disabled selected value="">Select Manager</option>
                                <%while (rs4.next()) {%>

                                <option value="<%=rs4.getString("id")%>"><%=rs4.getString("name")%></option>
                                <%}%>
                            </select>
                        </li>
                    </ul>
                    <%}%>
                    <ul class="d-flex">

                        <%--<li><h5>Employes</h5></li>--%>
                        <li class="search-form">
                            <form action="account-info" method="get">
                                <input type="text" name="employeeName" placeholder="Search here...">
                                <%--<input id="managerId" type="hidden" name="managerId" value="">--%>
                                <button type="submit">search</button>
                            </form>
                        </li>
                    </ul>


                <h2 class="section-title">Profile <span> </span></h2>
                <div class="table-responsive profile-info  table-style">
                    <table>

                        <thead>
                        <tr>
                            <%if((!Utils.isEmplyOrNull(request.getParameter("id")))){%>
                            <th>Photo</th>
                            <th>Select Photo</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Branch</th>
                            <th>User Type</th>
                            <th></th>
                            <th></th>
                            <%}else{%>
                            <th>S.No</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Id</th>
                            <th>Designation</th>
                            <th>Branch</th>
                            <th>Employee or Manager</th>
                            <th>Manager Name</th>
                            <th></th>
                            <th></th>
                            <%}%>
                        </tr>
                        </thead>
                        <tbody>
                        <%if(isAdmin || !Utils.isEmplyOrNull(request.getParameter("id"))) {%>
                        <form method="post" action="profile" enctype="multipart/form-data">
                            <tr>
                                <td><img style="height: 80px; width: 100px;" src="resources/assets/images/<%=request.getParameter("id")%>.jpg"></td>
                                <td>
                                    <input type="file" class="form-control btn btn-file" name="file" placeholder="photo" required>
                                </td>
                                <td>
                                    <input class="form-control" name="username" placeholder="username" value="<%=username%>" required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="password" value="<%=password%>" placeholder="password">
                                </td>
                                <td><input class="form-control" name="name" value="<%=name%>" placeholder="name"></td>
                                <td><input class="form-control" name="designation" value="<%=designation%>" placeholder="designation"></td>
                                <td><input class="form-control" name="branch" value="<%=branch%>" placeholder="branch"></td>
                                <%if(isAdmin){%>
                                <td>
                                    <select name="user_type" class="form-control" value="<%=user_type%>" onchange="javascript:showOrHideManager();" required >
                                        <option value="">--Select User Type--</option>
                                        <%if(isAdmin){%><option value="manager">Manager</option><%}%>
                                        <option value="employee">Employee</option>
                                    </select>

                                    <%
                                        String q3 = "select * from user where user_type='manager'";
                                        Statement st3 = db.connection.createStatement();
                                        ResultSet rs3 = st3.executeQuery(q3);
                                    %>
                                </td>
                                <td>
                                    <select class="form-control" name="manager" value="<%=assigned_manager%>" id="managerD" style="display: none;">
                                        <option value="">--Select Assigned to Manager--</option>
                                        <%while (rs3.next()) {%>
                                        <option value="<%=rs3.getString("id")%>"><%=rs3.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <%}else{%>
                                <input type="hidden" name="user_type" value="employee">
                                <input type="hidden" name="manager" value="<%=session.getAttribute("id")%>">
                                <%}%>
                                <%if(Utils.isEmplyOrNull(request.getParameter("id"))){
                                if(isAdmin){
                                %>

                                <input type="hidden" name="insert" value="true">
                                <%}}else{%>
                                <input type="hidden" name="updateProductId" value="<%=request.getParameter("id")%>">
                                <%}%>

                                <td class="addmenu"><button type="submit"><i class="fa fa-plus"></i></button></td>
                            </tr>
                        </form>

                        <%}%>
                        <%if(!(!Utils.isEmplyOrNull(request.getParameter("id")))){%>
                        <%
                            int cnt=1;
                            Statement st5 = db.connection.createStatement();

                            String exQ="";
                            if(!Utils.isEmplyOrNull(request.getParameter("manager"))) {
                                exQ += " and assigned_manager="+request.getParameter("manager");
                            }

                            if(!isAdmin) {
                                exQ+=" and assigned_manager="+session.getAttribute("id");
                            }

                            if(request.getParameter("employeeName")!=null && !request.getParameter("employeeName").equals("")) {
                                exQ+=" and name like '%"+request.getParameter("employeeName")+"%'";
                            }

                            ResultSet rs5 = st5.executeQuery("select * from user where  (user_type='employee' or user_type='manager') "+exQ);

                            while (rs5.next()) {

                        %>
                        <tr>
                            <td><%=cnt++%></td>
                            <td><img style="height: 80px; width: 100px;" src="resources/assets/images/<%=rs5.getString("id")%>.jpg"></td>
                            <td><%=rs5.getString("name")%></td>
                            <td><%=rs5.getString("id")%></td>
                            <td><%=rs5.getString("designation")%></td>
                            <td><%=rs5.getString("branch")%></td>
                            <td><%=rs5.getString("user_type")%></td>
                            <td>
                            <%
                                Statement st6 = db.connection.createStatement();
                            ResultSet rs6 = st6.executeQuery("select * from user where id="+rs5.getString("assigned_manager"));

                            if(rs6.next() && rs5.getString("user_type").equals("employee")) {%>
                                <%=rs6.getString("name")%>
                            <%}
                            %>


                            </td>
                            <td class="addmenu"><a href="account-info?id=<%=rs5.getString("id")%>"><i class="fa fa-edit"></i></a></td>
                            <%if(isAdmin) {%>

                            <form action="profile" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="delete" value="<%=rs5.getString("id")%>">
                                <td class="addmenu"><button type="submit"><i class="fa fa-minus"></i></button></td>
                            </form>
                            <%}%>

                        </tr>
                        <%}}%>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>

</html>