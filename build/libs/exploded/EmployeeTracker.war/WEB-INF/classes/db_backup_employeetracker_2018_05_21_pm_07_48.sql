-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: employeetracker
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event_type` varchar(110) NOT NULL,
  `latitude` varchar(110) NOT NULL,
  `longitude` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `event_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,2,'2018-05-09 11:02:00','start','23.9','91.4'),(2,2,'2018-05-09 11:02:35','active','23.1','91.9'),(3,2,'2018-05-09 11:04:03','pause','23.111','91.9'),(4,2,'2018-05-09 11:04:26','end','23.56','93.9'),(5,3,'2018-05-08 11:03:49','start','22.99','91.99'),(6,3,'2018-04-14 11:04:43','active','23.5','89.9'),(7,3,'2018-04-14 17:22:58','end','21.22','91.25'),(8,2,'2018-04-16 21:53:56','end','24.0','92.3'),(9,2,'2018-04-14 11:04:26','start','23.4','99.3'),(10,2,'2018-04-14 12:04:26','active','25','100'),(11,15,'2018-04-25 17:02:18','start','26.7','102.5'),(12,15,'2018-04-25 17:03:33','resume','23.111','91.25'),(13,15,'2018-04-25 17:04:33','end','24.0','91.4'),(25,8,'2018-05-10 18:45:20','start','24.90767','91.95009'),(26,8,'2018-05-10 18:46:21','active','24.89842','91.94887'),(27,8,'2018-05-10 18:47:24','pause','24.89842','91.94887'),(28,8,'2018-05-10 18:47:31','resume','24.89842','91.94887'),(30,8,'2018-05-10 18:54:57','start','24.90767','91.95009'),(31,8,'2018-05-10 18:54:59','end','24.90767','91.95009'),(32,8,'2018-05-10 18:55:09','start','24.90767','91.95009'),(33,8,'2018-05-10 18:55:28','pause','24.90767','91.95009'),(34,8,'2018-05-10 18:55:50','resume','24.90767','91.95009'),(35,8,'2018-05-10 18:56:09','active','24.90767','91.95009'),(37,8,'2018-05-10 18:58:01','start','24.89842','91.94887'),(38,8,'2018-05-10 18:58:12','pause','24.89842','91.94887'),(39,8,'2018-05-10 18:58:15','end','24.89842','91.94887'),(40,8,'2018-05-10 18:58:20','start','24.90767','91.95009'),(41,8,'2018-05-10 18:58:25','end','24.90767','91.95009'),(42,8,'2018-05-11 03:44:46','start','24.89842','91.94887'),(43,8,'2018-05-11 03:45:02','active','24.89842','91.94887'),(44,8,'2018-05-11 03:45:07','end','24.89842','91.94887'),(45,8,'2018-05-11 03:46:03','start','24.90767','91.95009'),(46,8,'2018-05-11 03:46:09','pause','24.90767','91.95009'),(47,8,'2018-05-11 03:48:24','end','24.90767','91.95009'),(48,8,'2018-05-11 03:52:42','start','24.89842','91.94887'),(49,8,'2018-05-11 03:52:47','pause','24.89842','91.94887'),(50,8,'2018-05-11 03:53:04','end','24.91285','91.95356'),(51,8,'2018-05-11 03:53:24','start','24.89842','91.94887'),(52,8,'2018-05-11 03:53:38','end','24.89842','91.94887'),(53,8,'2018-05-11 03:53:42','start','24.89842','91.94887'),(54,8,'2018-05-11 03:53:45','pause','24.89842','91.94887'),(55,8,'2018-05-11 03:53:54','end','24.89842','91.94887'),(56,7,'2018-05-11 10:23:55','start','16.79328','81.52615'),(57,7,'2018-05-11 10:23:55','start','16.79328','81.52615'),(58,7,'2018-05-11 10:24:14','active','16.79328','81.52615'),(59,7,'2018-05-11 10:24:24','active','16.7934','81.5258'),(60,7,'2018-05-11 10:24:34','active','16.79329','81.52611'),(61,7,'2018-05-11 10:24:45','active','16.79313','81.52583'),(62,7,'2018-05-11 10:25:11','active','16.79332','81.52604'),(63,7,'2018-05-11 10:25:36','active','16.79434','81.52612'),(64,7,'2018-05-11 10:25:46','active','16.79502','81.52607'),(65,7,'2018-05-11 10:25:56','active','16.79543','81.52609'),(66,7,'2018-05-11 10:26:05','active','16.79491','81.527'),(67,7,'2018-05-11 10:26:16','active','16.79468','81.52737'),(68,7,'2018-05-11 10:26:25','active','16.79449','81.52803'),(69,7,'2018-05-11 10:26:35','active','16.79593','81.52795'),(70,7,'2018-05-11 10:26:46','active','16.7968','81.52809'),(71,7,'2018-05-11 10:26:56','active','16.79762','81.5284'),(72,7,'2018-05-11 10:27:05','active','16.7984','81.52868'),(73,7,'2018-05-11 10:27:15','active','16.79923','81.52898'),(74,7,'2018-05-11 10:27:25','active','16.80023','81.52902'),(75,7,'2018-05-11 10:27:35','active','16.80101','81.52943'),(76,7,'2018-05-11 10:27:45','active','16.80185','81.52964'),(77,7,'2018-05-11 10:27:56','active','16.80264','81.53001'),(78,7,'2018-05-11 10:28:05','active','16.80349','81.53005'),(79,7,'2018-05-11 10:28:16','active','16.80342','81.53025'),(80,7,'2018-05-11 10:28:26','active','16.80324','81.53004'),(81,7,'2018-05-11 10:28:42','active','16.80331','81.52986'),(82,7,'2018-05-11 10:28:51','active','16.80329','81.5297'),(83,7,'2018-05-11 10:29:35','active','16.80312','81.53025'),(84,7,'2018-05-11 10:31:15','active','16.80314','81.53008'),(85,7,'2018-05-11 10:31:56','active','16.80312','81.53025'),(86,7,'2018-05-11 10:32:58','active','16.8033','81.52973'),(87,7,'2018-05-11 10:33:19','active','16.80324','81.52997'),(88,7,'2018-05-11 10:33:29','active','16.80218','81.52978'),(89,7,'2018-05-11 10:33:42','active','16.80107','81.52943'),(90,7,'2018-05-11 10:33:52','active','16.79998','81.5292'),(91,7,'2018-05-11 10:34:02','active','16.79928','81.5289'),(92,7,'2018-05-11 10:34:12','active','16.79839','81.5287'),(93,7,'2018-05-11 10:34:22','active','16.79773','81.52857'),(94,7,'2018-05-11 10:34:32','active','16.7967','81.52819'),(95,7,'2018-05-11 10:34:42','active','16.79572','81.52799'),(96,7,'2018-05-11 10:34:52','active','16.79484','81.52779'),(97,7,'2018-05-11 10:35:02','active','16.7942','81.52755'),(98,7,'2018-05-11 10:35:12','active','16.7941','81.52745'),(99,7,'2018-05-11 10:35:22','active','16.79533','81.52622'),(100,7,'2018-05-11 10:35:32','active','16.79461','81.52635'),(101,7,'2018-05-11 10:35:42','active','16.79373','81.52606'),(102,7,'2018-05-11 10:35:52','active','16.79339','81.52611'),(103,7,'2018-05-11 10:36:12','active','16.7934','81.52599'),(104,7,'2018-05-11 10:37:58','active','16.79329','81.52611'),(105,7,'2018-05-11 10:38:33','pause','16.79329','81.52611'),(106,7,'2018-05-11 10:39:06','end','16.79329','81.52611'),(107,7,'2018-05-11 10:39:32','start','16.79329','81.52611'),(108,7,'2018-05-11 10:39:32','start','16.79329','81.52611'),(109,7,'2018-05-11 10:40:02','pause','16.79329','81.52611'),(110,7,'2018-05-11 10:40:21','end','16.79329','81.52611'),(111,14,'2018-05-12 06:51:06','pause','0.0','0.0'),(112,7,'2018-05-12 08:50:22','start','16.71179','81.10622'),(113,7,'2018-05-12 08:50:32','active','16.71179','81.10622'),(114,7,'2018-05-12 08:52:03','active','16.71155','81.10629'),(115,7,'2018-05-12 08:52:12','active','16.71144','81.10629'),(116,7,'2018-05-12 08:52:38','active','16.71179','81.10622'),(117,7,'2018-05-12 08:53:28','active','16.71133','81.10635'),(118,7,'2018-05-12 08:54:23','active','16.71179','81.10622'),(119,7,'2018-05-12 08:55:38','active','16.71133','81.10635'),(120,7,'2018-05-12 08:55:53','active','16.71179','81.10622'),(121,7,'2018-05-12 08:56:53','active','16.71133','81.10635'),(122,7,'2018-05-12 08:57:08','active','16.71179','81.10622'),(123,7,'2018-05-12 08:58:13','active','16.71133','81.10635'),(124,7,'2018-05-12 08:58:38','active','16.71179','81.10622'),(125,7,'2018-05-12 09:01:13','active','16.71133','81.10635'),(126,7,'2018-05-12 09:01:23','active','16.71179','81.10622'),(127,7,'2018-05-12 09:01:38','active','16.71133','81.10635'),(128,7,'2018-05-12 09:02:33','active','16.71179','81.10622'),(129,7,'2018-05-12 09:05:19','active','16.71144','81.10646'),(130,7,'2018-05-12 09:05:47','active','16.71179','81.10622'),(131,7,'2018-05-12 09:13:13','active','16.71208','81.10564'),(132,7,'2018-05-12 09:14:18','active','16.71175','81.10604'),(133,7,'2018-05-12 09:14:39','active','16.71179','81.10622'),(134,7,'2018-05-12 09:15:43','active','16.71151','81.10623'),(135,7,'2018-05-12 09:16:03','active','16.71179','81.10622'),(136,7,'2018-05-12 09:16:23','active','16.71191','81.1062'),(137,7,'2018-05-12 09:16:43','active','16.71185','81.1052'),(138,7,'2018-05-12 09:17:08','active','16.7116','81.10421'),(139,7,'2018-05-12 09:17:28','active','16.7115','81.1033'),(140,7,'2018-05-12 09:17:48','active','16.7111','81.10173'),(141,7,'2018-05-12 09:17:58','active','16.71119','81.1014'),(142,7,'2018-05-12 09:18:13','active','16.71069','81.10061'),(143,7,'2018-05-12 09:18:23','active','16.71043','81.09993'),(144,7,'2018-05-12 09:18:48','active','16.70979','81.09807'),(145,7,'2018-05-12 09:18:58','active','16.71001','81.09788'),(146,7,'2018-05-12 09:19:14','active','16.71113','81.09754'),(147,7,'2018-05-12 09:19:23','active','16.71174','81.09744'),(148,7,'2018-05-12 09:19:38','active','16.71217','81.09741'),(149,7,'2018-05-12 09:19:48','active','16.71271','81.09726'),(150,7,'2018-05-12 09:20:49','active','16.71275','81.0971'),(151,7,'2018-05-12 09:23:20','active','16.7128','81.09699'),(152,7,'2018-05-12 09:24:19','active','16.71275','81.09714'),(153,7,'2018-05-12 09:27:13','active','16.71268','81.09705'),(154,7,'2018-05-12 09:34:28','active','16.71269','81.09707'),(155,7,'2018-05-12 09:48:30','active','16.71168','81.1064'),(156,7,'2018-05-12 09:57:43','end','16.71168','81.1064'),(157,7,'2018-05-14 04:38:41','start','16.71179','81.10622'),(158,7,'2018-05-14 04:38:56','active','16.71179','81.10622'),(159,7,'2018-05-14 04:40:19','active','16.7111','81.10679'),(160,7,'2018-05-14 04:40:24','active','16.71179','81.10622'),(161,7,'2018-05-14 04:40:49','active','16.71128','81.10635'),(162,7,'2018-05-14 04:41:03','active','16.71028','81.10677'),(163,7,'2018-05-14 04:41:29','active','16.71036','81.10805'),(164,7,'2018-05-14 04:41:44','active','16.71051','81.10875'),(165,7,'2018-05-14 04:42:03','active','16.71076','81.11053'),(166,7,'2018-05-14 04:42:13','active','16.71073','81.11099'),(167,7,'2018-05-14 04:42:34','active','16.71098','81.11102'),(168,7,'2018-05-14 04:42:54','active','16.71141','81.11101'),(169,7,'2018-05-14 04:43:04','active','16.71108','81.11101'),(170,7,'2018-05-14 04:43:45','active','16.71072','81.10949'),(171,7,'2018-05-14 04:43:54','active','16.71036','81.1086'),(172,7,'2018-05-14 04:44:19','active','16.71009','81.10739'),(173,7,'2018-05-14 04:44:31','active','16.70953','81.10563'),(174,7,'2018-05-14 04:44:55','active','16.70937','81.10453'),(175,7,'2018-05-14 04:44:58','active','16.7094','81.10473'),(176,7,'2018-05-14 04:45:29','active','16.70983','81.10629'),(177,7,'2018-05-14 04:45:38','active','16.71055','81.1067'),(178,7,'2018-05-14 04:45:58','active','16.71068','81.10637'),(179,7,'2018-05-14 04:48:14','active','16.71246','81.10599'),(180,7,'2018-05-14 04:50:39','active','16.71068','81.10637'),(181,7,'2018-05-14 04:51:09','active','16.71133','81.10635'),(182,7,'2018-05-14 04:53:14','active','16.71068','81.10637'),(183,7,'2018-05-14 04:53:39','active','16.71133','81.10635'),(184,7,'2018-05-14 04:55:09','active','16.71068','81.10637'),(185,7,'2018-05-14 04:56:29','active','16.7111','81.10679'),(186,7,'2018-05-14 04:56:44','active','16.71068','81.10637'),(187,7,'2018-05-14 04:57:34','active','16.71114','81.10659'),(188,7,'2018-05-14 04:57:59','active','16.71133','81.10635'),(189,7,'2018-05-14 04:58:24','active','16.7111','81.10679'),(190,7,'2018-05-14 04:58:50','active','16.71179','81.10622'),(191,7,'2018-05-14 04:59:13','end','16.71179','81.10622'),(192,7,'2018-05-14 04:59:37','start','16.71179','81.10622'),(193,7,'2018-05-14 04:59:42','end','16.71179','81.10622');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `designation` varchar(110) DEFAULT NULL,
  `details` varchar(110) DEFAULT NULL,
  `user_type` varchar(110) NOT NULL DEFAULT 'employee',
  `assigned_manager` int(11) DEFAULT '0',
  `branch` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','et1','Admin','CEO','Owner of the Tracking Company','admin',1,'a'),(2,'slogin','123456','SMR','HR',NULL,'manager',1,'ELURU'),(3,'ilogin','123456','indrakanth','Manager',NULL,'manager',1,'Vizag'),(4,'nlogin','123456','naga','Manager',NULL,'manager',1,'TPG'),(5,'glogin','123456','Gayathri','Manager',NULL,'manager',1,'TPG'),(6,'rlogin','123456','Suri reddy','Manager',NULL,'manager',1,'TPG'),(7,'suser1','123456','Auser','Marketing',NULL,'employee',2,'Eluru'),(8,'suser2','123456','Buser','Marketing',NULL,'employee',2,'Eluru'),(9,'suser3','123456','Cuser','Marketing',NULL,'employee',2,'Eluru'),(10,'suser4','123456','Duser','Marketing',NULL,'employee',2,'Eluru'),(11,'suser5','123456','Euser','Marketing',NULL,'employee',2,'Eluru'),(12,'iuser1','123456','Ninja','Manager',NULL,'employee',3,'Vizag'),(13,'nuser1','123456','Sai','Marketing',NULL,'employee',4,'TPG'),(14,'ruser1','123456','Reddy','Marketing',NULL,'employee',6,'TPG'),(15,'guser1','123456','Nimshitha','Senior Hr',NULL,'employee',5,'TPG');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-21  6:49:57
