(function($) {
    "use strict";
    // responsive-menu tigger
    $(".menu").click(function() {
        $(".sliderbar-area,.menu").toggleClass("active");
    });

    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    $('.js-example-basic-single').select2();

    // click
    function DropDown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('ul.dropdown > li');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    DropDown.prototype = {
        initEvents: function() {
            var obj = this;

            obj.dd.on('click', function(event) {
                $(this).toggleClass('active');
                return false;
            });

            obj.opts.on('click', function() {
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                obj.placeholder.text(obj.val);
            });
        },
        getValue: function() {
            return this.val;
        },
        getIndex: function() {
            return this.index;
        }
    }

    $(function() {

        var dd = new DropDown($('.select-menu'));

        $(document).click(function() {
            // all dropdowns
            $('.select-menu').removeClass('active');
        });

    });


   



 
    $(window).on('load', function() {

    });



    $(window).on("scroll", function() {
    });

})(jQuery);